% Setting for figure 7

%%% Generate noise-free matrix
m = 200;
n = 100;
mn = min(m, n);
alpha = 4/mn;
for k = 1:m
    w = exp(-((1:n)-n).^2/n^2*6) .* exp(-(k-m).^2/m^2*6);
    X(k, :) = ...
        abs(cos(alpha*k*2*pi).*sin(alpha*(1:n)*2*pi)) .* ...
        (1-w) + 300*w;
end
[U, si, V] = mysvd(X);
si(6:end) = 0;
X = U(:, 1:mn) * diag(si) * V(:, 1:mn)';
X = 800 * X / mean(X(:));
si = svd(X);

%% Setting of singular value estimators
sv_est.name{1}   = 'PCA (active set)';
sv_est.symb{1}   = '$\hat{\bf X}^r$';
sv_est.style{1}  = { 'Color', [1 0.5 0], 'LineStyle', ':', 'LineWidth', 2 };
sv_est.opt{1}    = { 'class',     'optweights', ...
                     'objective', 'pca_poisson' };

sv_est.name{2}   = 'ST MSE';
sv_est.symb{2}   = 'SE$_\eta$ ${\bf X}_{\mathrm{soft}}$';
sv_est.style{2}  = { 'Color', 'c' };
sv_est.opt{2}    = { 'class',     'softtresholding', ...
                     'objective', 'mse_poisson' };

sv_est.name{3}   = 'ST PURE';
sv_est.symb{3}   = 'PURE $\hat{\bf X}_{\mathrm{soft}}$';
sv_est.style{3}  = { '--', 'Color', 'c', 'LineWidth', 2 };
sv_est.opt{3}    = { 'class',     'softtresholding', ...
                     'objective', 'pure' };

sv_est.name{4}   = 'ST KLA';
sv_est.symb{4}   = 'KLA ${\bf X}_{\mathrm{soft}}$';
sv_est.style{4}  = { 'Color', 'm' };
sv_est.opt{4}    = { 'class',     'softtresholding', ...
                     'objective', 'kla' };

sv_est.name{5}   = 'ST PUKLA';
sv_est.symb{5}   = 'PUKLA $\hat{\bf X}_{\mathrm{soft}}$';
sv_est.style{5}  = { '--', 'Color', 'm', 'LineWidth', 2 };
sv_est.opt{5}    = { 'class',     'softtresholding', ...
                     'objective', 'pukla' };

sv_est.name{6}   = 'Optimal MSE';
sv_est.symb{6}   = 'SE$_\eta$ ${\bf X}_w^r$';
sv_est.style{6}  = { 'Color', 'g' };
sv_est.opt{6}    = { 'class',     'optweights', ...
                     'objective', 'mse_poisson' };

sv_est.name{7}   = 'Optimal PURE';
sv_est.symb{7}   = 'PURE $\hat{\bf X}_w^r$';
sv_est.style{7}  = { '--', 'Color', 'g', 'LineWidth', 2 };
sv_est.opt{7}    = { 'class',     'optweights', ...
                     'objective', 'pure' };

sv_est.name{8}   = 'Optimal KLA';
sv_est.symb{8}   = 'KLA ${\bf X}_w^r$';
sv_est.style{8}  = { 'Color', 'k' };
sv_est.opt{8}    = { 'class',     'optweights', ...
                     'objective', 'kla' };

sv_est.name{9}   = 'Optimal PUKLA';
sv_est.symb{9}   = 'PUKLA $\hat{\bf X}_w^r$';
sv_est.style{9}  = { '--', 'Color', 'k', 'LineWidth', 2 };
sv_est.opt{9}    = { 'class',     'optweights', ...
                     'objective', 'pukla' };

K                = length(sv_est.name);

%% Thresholding of matrix elements
eps              = 1e-6;
