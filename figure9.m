clear all
close all

addpath('tools');
deterministic('on');

%%% Number of simulations for Monte-Carlo
M = 400;

%%% Apply restriction to the bulk edge
rank_method = 'bulk_edge';

%% Generate noise-free matrix
X = double(imread('mandrill.png'));
X = double(imresize(X, [256, 256]));
name = 'mandrill';

[U, si, V] = mysvd(X);

set(0,'defaultTextInterpreter','latex');
f = figure;
a = loglog(si, 'r', 'LineWidth', 2);
ylim([0.2, 10^5]);
xlabel('Indices')
ylabel('Singular values')
box on
waitfor(f);

Xg = double(imresize(X, [300, 300]));

%% Setting of singular value estimators
sv_est.name{1}   = 'Nadakuditi';
sv_est.symb{1}   = '$\hat{\bf X}_*$';
sv_est.style{1}  = { ':', 'Color', [210, 105, 30]/255, 'LineWidth', 2 };
sv_est.opt{1}    = { 'class',     'optweights', ...
                     'objective', 'nadakuditi' };

sv_est.name{2}   = 'Optimal SURE';
sv_est.symb{2}   = '$\hat{\bf X}_w^r$';
sv_est.style{2}  = { '--', 'Color', 'k', 'LineWidth', 2 };
sv_est.opt{2}    = { 'class',     'optweights', ...
                    'objective', 'sure' };

K                = length(sv_est.name);

%%% Computation
L = 10;
l = round(sqrt(linspace(20^2, 250^2, L)));
for rsnr = [10 7 5]

    %%% Noise standard deviation
    tau = std(Xg(:)) / rsnr;

    error_init       = zeros(L, M);
    for k = 1:K
        error_est{k} = zeros(L, M);
    end
    hw = waitbar(0);
    for i = 1:L
        waitbar(i / L, hw);

        m = l(i);
        n = l(i);
        mn = min(m, n);
        X = imresize(Xg, [m, n]);
        [U, si, V] = mysvd(X);
        X = U(:, 1:mn)*diag(si(1:mn))*V(:, 1:mn)';
        si = svd(X);

        for q = 1:M
            % Generate noisy matrix
            Y            = X + tau * randn(m, n);
            [Ut, st, Vt] = mysvd(Y);

            % Compute initial error
            error_init(i, q)    = nmse(Y, X);

            % Optimal rank MSE and KLA
            rh = rank_estimation(m, n, tau, 'st', st, 'si', si, ...
                                 'rank_method', rank_method);

            for k = 1:K
                % Estimates of singular values
                sh{k}    = sv_shrinkage(st, sv_est.opt{k}{:}, ...
                                        'noise', 'gaussian', ...
                                        'Ut', Ut, 'Vt', Vt, 'X', X, ...
                                        'm',  m,  'n',  n,  'tau', tau, ...
                                        'rank', rh);

                % Compute approximations from singular values
                Xh{k} = Ut(:, 1:mn) * diag(sh{k}(1:mn)) * Vt(:, 1:mn)';
                error_est{k}(i, q)  = nmse(Xh{k}, X);
            end
        end
    end
    close(hw);

    %%% Display
    f = fancyfigure;
    a = [];
    for k = 1:K
        a = [a plotuncertainty(l, error_est{k}, sv_est.style{k}{:})];
    end
    h = fancylegend(a, sv_est.symb{:}, ...
                    'Location', 'NorthEast');
    xlabel('Size $n$')
    ylabel('NMSE')
    xlim([l(1) l(end)])
    box
    waitfor(f);
end
