% Setting for figure 5

%%% Generate noise-free matrix
m = 200;
n = 100;
mn = min(m, n);
alpha = 4/mn;
for k = 1:m
    w = exp(-((1:n)-n).^2/n^2*6) .* exp(-(k-m).^2/m^2*6);
    X(k, :) = ...
        abs(cos(alpha*k*2*pi).*sin(alpha*(1:n)*2*pi)) .* ...
        (1-w) + 100*w;
end
X = log(X) - min(log(X(:)))*1.1;
[U, si, V] = mysvd(X);
si(6:end) = 0;
X = U(:, 1:mn)*diag(si)*V(:, 1:mn)';
X = 800 * X / mean(X(:));
si = svd(X);
si(6:end) = 0;

%% Setting of singular value estimators
sv_est.name{1}   = 'PCA';
sv_est.symb{1}   = '$\hat{\bf X}^r$';
sv_est.style{1}  = { 'Color', 'g' };
sv_est.opt{1}    = { 'class',     'optweights', ...
                     'objective', 'pca' };

sv_est.name{2}   = 'Nadakuditi';
sv_est.symb{2}   = '$\hat{\bf X}_*$';
sv_est.style{2}  = { ':', 'Color', [210, 105, 30]/255, 'LineWidth', 2 };
sv_est.opt{2}    = { 'class',     'optweights', ...
                     'objective', 'nadakuditi' };

sv_est.name{3}   = 'ST MSE';
sv_est.symb{3}   = '${\bf X}_{\mathrm{soft}}$';
sv_est.style{3}  = { 'Color', 'm' };
sv_est.opt{3}    = { 'class',     'softtresholding', ...
                     'objective', 'mse' };

sv_est.name{4}   = 'ST SURE';
sv_est.symb{4}   = '$\hat{\bf X}_{\mathrm{soft}}$';
sv_est.style{4}  = { '--', 'Color', 'm', 'LineWidth', 2 };
sv_est.opt{4}    = { 'class',     'softtresholding', ...
                     'objective', 'sure' };

sv_est.name{5}   = 'Optimal MSE';
sv_est.symb{5}   = '${\bf X}_w^r$';
sv_est.style{5}  = { 'Color', 'k' };
sv_est.opt{5}    = { 'class',     'optweights', ...
                     'objective', 'mse' };

sv_est.name{6}   = 'Optimal SURE';
sv_est.symb{6}   = '$\hat{\bf X}_w^r$';
sv_est.style{6}  = { '--', 'Color', 'k', 'LineWidth', 2 };
sv_est.opt{6}    = { 'class',     'optweights', ...
                     'objective', 'sure' };

K                = length(sv_est.name);
