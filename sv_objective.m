function cost = sv_objective(fsh, varargin)
%% Estimate some cost function of an spectral estimator
%
% Inputs/Outputs
%
%    FSH        an handle function returning the estimated singular values
%
%    COST       an handle function returning the cost
%               the input of the handle function can be anything
%               but it will be the same for FSH and COST
%
%    NOISE      a string being 'gaussian', 'gamma' or 'poisson'
%
% Optional arguments (but sometimes mandatory according to the COST)
%
%    OBJECTIVE  a string being
%               - 'mse', 'sure'*                                (Gaussian case)
%               - 'mse_gamma', 'gsure', 'kls', 'sukls'*         (Gamma case)
%               - 'mse_poisson', 'pure', 'kla', 'pukla'*        (Poisson case)
%               Default are marked with a * and depends on the noise
%
%    M          the number of rows of the matrix
%
%    N          the number of columns of the matrix
%
%    ST         the noisy singular values
%
%    Ut         the left singular vectors stored in columns
%
%    Vt         the right singular vectors stored in columns
%
%    X          the noise-free version of the matrix
%               (only required for oracle estimators)
%
%    FDSH       an handle function returning the derivaitve of the
%               FSH with respect to ST
%
%    FDDSH      an handle function returning the directional derivaitve of the
%               FSH with respect to ST in a direction DELTA
%
%    DELTA      a direction (same size as ST) for FDDSH
%               generated as (non random) Bernoulli distributed if not provided
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle & Jeremie Bigot
% Email charles-alban.deledalle@math.u-bordeaux.fr



options   = makeoptions(varargin{:});
noise     = getoptions(options, 'noise', '');

%% Retrieved required arguments wrt noise model
switch noise
    case 'gaussian'
        objective = getoptions(options, 'objective', 'sure');
        tau       = getoptions(options, 'tau', [], true);
    case 'gamma'
        objective = getoptions(options, 'objective', 'sukls');
        L         = getoptions(options, 'L', [], true);
        eps       = getoptions(options, 'eps', [], true);
    case 'poisson'
        objective = getoptions(options, 'objective', 'pukla');
        eps       = getoptions(options, 'eps', [], true);
    otherwise
        objective = getoptions(options, 'objective', 'mse');
end

%% Retrieved required arguments wrt objectives
switch objective
    case {'sure'}
        m      = getoptions(options, 'm', [], true);
        n      = getoptions(options, 'n', [], true);
        st     = getoptions(options, 'st', [], true);
        fdsh   = getoptions(options, 'fdsh', [], true);
        mn     = min(m, n);

        delta = bsxfun(@minus, repmat(st.^2, [1, mn]), st'.^2);
        delta = 1 ./ delta;
        delta(isinf(delta)) = 0;
        delta = st(:) .* sum(delta, 2);

    case {'sukls'}
        Ut     = getoptions(options, 'Ut', [], true);
        Vt     = getoptions(options, 'Vt', [], true);
        m      = size(Ut, 1);
        n      = size(Vt, 1);
        st     = getoptions(options, 'st', [], true);
        fdsh   = getoptions(options, 'fdsh', [], true);
        mn     = min(m, n);

        delta = bsxfun(@minus, repmat(st.^2, [1, mn]), st'.^2);
        delta = 1 ./ delta;
        delta(isinf(delta)) = 0;
        delta = st(:) .* sum(delta, 2);

    case {'gsure', 'pukla', 'pure'}
        Ut     = getoptions(options, 'Ut', [], true);
        Vt     = getoptions(options, 'Vt', [], true);
        st     = getoptions(options,  'st', [], true);
        fddsh  = getoptions(options, 'fddsh', [], true);
        m      = size(Ut, 1);
        n      = size(Vt, 1);
        mn     = min(m, n);

        delta = getoptions(options, 'delta', []);
        if isempty(delta)
            state = deterministic('on');
            delta = sign(randn(m, n));
            deterministic('off', state);
        end

    otherwise
        X      = getoptions(options, 'X', [], true);
        Ut     = getoptions(options, 'Ut', [], true);
        Vt     = getoptions(options, 'Vt', [], true);
        m      = size(Ut, 1);
        n      = size(Vt, 1);
        mn     = min(m, n);
end

switch noise
    case { 'gamma', 'poisson' }
        build = @(s) max(Ut(:, 1:mn) * diag(s) * Vt(:, 1:mn)', eps);
end
switch objective
    case { 'sukls', 'gsure', 'pure', 'pukla' }
        Xt = build(st);
end

%% Define objective
switch objective
    case 'mse'
        s      = diag(Ut(:, 1:mn)' * X * Vt(:, 1:mn));
        cost = @(sh) norm(sh - s)^2;
    case 'sure'
        cost = @(sh, dsh) ...
               norm(st - sh)^2 + 2 * tau^2 * ...
               sum(sum(abs(m-n) * (sh ./ st) ...
                       + dsh + 2 * sh .* delta));
    case 'kls'
        cost = @(Xh) L * sum(sum(Xh ./ X - log(Xh ./ X) - 1));
    case 'sukls'
        cost = @(Xh, sh, dsh) ...
               sum(sum((L-1) * Xh ./ Xt - L * log(Xh))) ...
               + sum(abs(m-n) * sh ./ st ...
                     + dsh + 2 * sh .* delta);
    case 'mse_gamma'
        cost   = @(Xh) L^2 * sum(sum((1 ./ Xh - 1 ./ X).^2));
    case 'gsure'
        cost  = @(Xh, ddXh) ...
                sum(sum(L^2./Xh.^2 - 2 * L * (L - 1) ./ Xt ./ Xh ...
                        + 2 * L * delta .* ddXh ./ Xh.^2));
    case 'kla'
        cost  = @(Xh) sum(sum(Xh - X - ylogx(X, Xh ./ X)));
    case 'pukla'
        cost  = @(Xh, ddXh) sum(sum(Xh - ylogx(Xt, max(Xh - delta .* ddXh, eps))));
    case 'mse_poisson'
        cost   = @(Xh) sum(sum((Xh - X).^2));
    case 'pure'
        cost  = @(Xh, ddXh) norm(Xh, 'fro')^2 ...
                - 2 * sum(sum(Xt .* max(Xh - delta .* ddXh, eps)));
    otherwise
        error(sprintf('Objective %s unknown', objective));
end

switch objective
    case { 'mse' }
        cost = @(p)       cost(fsh(p));
    case { 'sure' }
        cost = @(p)       cost(fsh(p), fdsh(p));
    case { 'sukls' }
        cost = @(sh, dsh) cost(build(sh), sh, dsh);
        cost = @(p)       cost(fsh(p), fdsh(p));
    case { 'mse_gamma', 'kls', 'mse_poisson', 'kla' }
        cost = @(p)       cost(build(fsh(p)));
    case { 'gsure', 'pure', 'pukla' }
        cost = @(p) pipe2(cost, ...
                          @(p) ddcompute(Ut, Vt, st, ...
                                         fsh(p), @(ds) fddsh(p, ds), ...
                                         delta, eps), ...
                          p);
end

% Compute directional_derivative
function [Xh, ddXh] = ddcompute(Ut, Vt, st, sh, fddsh, delta, eps)

    m = size(Ut, 1);
    n = size(Vt, 1);
    mn = min(m, n);

    Xh = Ut(:, 1:mn) * diag(sh) * Vt(:, 1:mn)';
    idx = Xh < eps;

    dbar  = Ut' * delta * Vt;
    dbart = zeros(m, n);
    dbart(1:min(mn, m), 1:min(mn, n)) = dbar(1:min(mn, n), 1:min(mn, m))';

    % Compute Jacobian applied to delta
    st_d  = [ st;     zeros(max(max(m, n)-mn, 0), 1)];
    sh = [ sh ; zeros(max(max(m, n)-mn, 0), 1)];
    H = zeros(m, n);
    H(1:mn, 1:mn)  = diag(fddsh(diag(dbar)));
    GS = (repmat(sh(1:m), [1 n]) - repmat(sh(1:n)', [m 1])) ./ ...
         (repmat(st_d(1:m), [1 n])  - repmat(st_d(1:n)', [m 1]));
    GS(isnan(GS)) = 0;
    GS = GS .* (dbar + dbart) / 2;
    GA = (repmat(sh(1:m), [1 n]) + repmat(sh(1:n)', [m 1])) ./ ...
         (repmat(st_d(1:m), [1 n])  + repmat(st_d(1:n)', [m 1]));
    GA(1:mn, 1:mn) = GA(1:mn, 1:mn) - diag(diag(GA));
    GA = GA .* (dbar - dbart) / 2;
    ddXh = Ut * (H + GS + GA) * Vt';

    % Thresholding
    Xh(idx) = eps;
    ddXh(idx) = 0;

function res = pipe2(f, g, x)

[a, b] = g(x);
res = f(a, b);

