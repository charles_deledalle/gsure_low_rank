clear all
close all

addpath('tools')
deterministic('on');

%%% Number of simulations for Monte-Carlo
M = 100;

%%% Number of discretization points of the rank
T = 20;

%%% Apply restriction to the active set
active_set_estimation = 'none';
%active_set_estimation = 'aic';

%%% Load setting
setting_figure6;

%%% Computation
rank_list     = unique(round(logspace(log10(1), log10(mn), T)));
T             = length(rank_list);
nmse_init     = zeros(T, M);
mkls_init     = zeros(T, M);
for k = 1:k
    nmse_est{k} = zeros(T, M);
    mkls_est{k} = zeros(T, M);
end
hw = waitbar(0);
for q = 1:M
    waitbar(q / M, hw);

    % Generate noisy matrix
    L = 80;
    Y = mygamrnd(X, L);
    [Ut, st, Vt] = mysvd(Y);
    if strcmp(active_set_estimation, 'aic')
        aset = sv_active_set(st, 'noise', 'gamma', ...
                             'Ut', Ut, 'Vt', Vt, 'L', L', 'eps', eps);
    else
        aset = 1:mn;
    end

    % Compute initial error
    nmse_init(:, q)    = nmse(Y, X);
    mkls_init(:, q)    = mkls(Y, X, L);

    for k = 1:K
        % Estimates of singular values
        sh{k}    = sv_shrinkage(st, sv_est.opt{k}{:}, ...
                                'noise', 'gamma', ...
                                'Ut', Ut, 'Vt', Vt, 'X', X, ...
                                'm',  m,  'n',  n,  'L', L, ...
                                'eps', eps, 'aset', aset);

        % Compute approximations from singular values
        if ~strcmp(sv_est.opt{k}{2}, 'optweights')
            Xh{k} = max(Ut(:, 1:mn) * diag(sh{k}) * Vt(:, 1:mn)',eps);
            nmse_est{k}(:, q)  = nmse(Xh{k}, X);
            mkls_est{k}(:, q)  = mkls(Xh{k}, X, L);
        else
            for i = 1:T
                r = rank_list(i);
                Xh{k} = max(Ut(:, 1:r) * diag(sh{k}(1:r)) * Vt(:, 1:r)', eps);
                nmse_est{k}(i, q) = nmse(Xh{k}, X);
                mkls_est{k}(i, q) = mkls(Xh{k}, X, L);
            end
        end
    end
end
close(hw);

%%% Display and save to /tmp/

% Figure 6.l-m
f = fancyfigure;
plotuncertainty(rank_list, nmse_init, 'Color', 'b');
for k = 1:K
    plotuncertainty(rank_list, nmse_est{k}, sv_est.style{k}{:});
end
switch active_set_estimation
    case 'none'
        xlabel('Rank $r$ (no restriction)')
    case 'aic'
        xlabel('Rank $r$ (restricted to the active set)')
end
ylabel('NMSE')
xlim([1, mn])
ylim([1.5*10^-3/3 2*10^-2]);
box on
set(gca,'XScale','log','YScale','log')
switch active_set_estimation
    case 'none'
        savefig(f, '/tmp/fig6m.fig');
    case 'aic'
        savefig(f, '/tmp/fig6l.fig');
end
waitfor(f);

% Figure 6.n-o
f = fancyfigure;
plotuncertainty(rank_list, mkls_init, 'Color', 'b');
for k = 1:K
    plotuncertainty(rank_list, mkls_est{k}, sv_est.style{k}{:});
end
switch active_set_estimation
    case 'none'
        xlabel('Rank $r$ (no restriction)')
    case 'aic'
        xlabel('Rank $r$ (restricted to the active set)')
end
ylabel('MKLS')
xlim([1, mn])
ylim([1.1*10^3 1.3*10^4]/m/n);
box on
set(gca,'XScale','log','YScale','log')
switch active_set_estimation
    case 'none'
        savefig(f, '/tmp/fig6o.fig');
    case 'aic'
        savefig(f, '/tmp/fig6n.fig');
end
