clear all
close all

addpath('tools');
deterministic('on');

%%% Number of simulations for Monte-Carlo
M = 1000;

%%% Load setting
setting_figure5;

%%% Computation
sst              = zeros(mn, M);
for k = 1:K
    ssh{k}       = zeros(mn, M);
end
hw = waitbar(0);
for q = 1:M
    waitbar(q / M, hw);

    % Generate noisy matrix
    tau          = 80;
    Y            = X + tau * randn(m, n);
    [Ut, st, Vt] = mysvd(Y);
    aset         = sv_active_set(st, 'noise', 'gaussian', ...
                                 'm', m, 'n', n, 'tau', tau);

    % Average spectrum of input
    sst(:,q)     = st;

    for k = 1:K
        % Estimates of singular values
        sh{k}    = sv_shrinkage(st, sv_est.opt{k}{:}, ...
                                'noise',  'gaussian', ...
                                'Ut', Ut, 'Vt', Vt, 'X', X, ...
                                'm',  m,  'n',  n,  'tau', tau, ...
                                'aset', aset);

        % Compute approximations from singular values
        Xh{k}    = Ut(:, 1:mn) * diag(sh{k}) * Vt(:, 1:mn)';

        % Average spectrum of output
        ssh{k}(:,q)   = svd(Xh{k});
    end
end
close(hw);

%%% Display and save to /tmp/

% Figure 5.h
f = fancyfigure;
a = loglog(max(si, 1e-20), 'r');
hold on
a = [a plotuncertainty(1:mn, max(sst, 1e-20), 'Color', 'b')];
for k = 1:K
    a = [a plotuncertainty(1:mn, max(ssh{k}, 1e-20), sv_est.style{k}{:})];
end
ylim([si(5)/100 2*si(1)]);
h = legend(a, '$\bf X$', '$\bf Y$', sv_est.symb{:}, ...
           'Location', 'NorthEast');
xlabel('Indices')
ylabel('Singular values')
set(h,'Interpreter','latex');
savefig(f, '/tmp/fig5h.fig');
waitfor(f);

% Figures 5.a to h
range = [min(X(:)), max(X(:))];
f = fancyfigure;
subplot(2,4,1)
plotimage(X, range);
title('Original matrix');
subplot(2,4,2)
plotimage(Y, range);
title('Noisy matrix');
for k = 1:K
    subplot(2,4,2+k)
    plotimage(Xh{k}, range);
    title(sprintf('%s', sv_est.name{k}));
end
linkaxes
savesubfig(f, '/tmp/fig5a-h/');
