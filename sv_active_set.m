function idx = sv_active_set(st, varargin)
%% Estimate the active set of singular values
%
% Inputs/Outputs
%
%    ST         the noisy singular values
%
%    IDX        the indices of active singular values
%
%    NOISE      a string being 'gaussian', 'gamma' or 'poisson'
%
% Optional arguments (but sometimes mandatory according to the COST)
%
%    M          the number of rows of the matrix
%
%    N          the number of columns of the matrix
%
%    Ut         the left singular vectors stored in columns
%
%    Vt         the right singular vectors stored in columns
%
%    X          the noise-free version of the matrix
%               (only required for oracle estimators)
%
%    TAU        standard deviation of the Gaussian noise
%
%    L          shape parameter of the Gamma distribution
%
%    eps        a priori on the smallest entry of the noise-free matrix
%
%    See also rank_estimation
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle & Jeremie Bigot
% Email charles-alban.deledalle@math.u-bordeaux.fr



%%
options   = makeoptions(varargin{:});
noise     = getoptions(options, 'noise', '');

%% Retrieve required arguments wrt noise model
switch noise
    case 'gaussian'
        tau    = getoptions(options, 'tau', [], true);
        m      = getoptions(options, 'm', [], true);
        n      = getoptions(options, 'n', [], true);
        mn     = min(m, n);
    case 'gamma'
        Ut     = getoptions(options, 'Ut', [], true);
        Vt     = getoptions(options, 'Vt', [], true);
        m      = size(Ut, 1);
        n      = size(Vt, 1);
        mn     = min(m, n);
        L      = getoptions(options, 'L', [], true);
        eps    = getoptions(options, 'eps', [], true);
    case 'poisson'
        Ut     = getoptions(options, 'Ut', [], true);
        Vt     = getoptions(options, 'Vt', [], true);
        m      = size(Ut, 1);
        n      = size(Vt, 1);
        mn     = min(m, n);
        eps    = getoptions(options, 'eps', [], true);
    otherwise
        error(sprintf('Noise %s unknown', noise));
end

switch noise
    case 'gaussian'
        r = getoptions(options, 'rank', []);
        if isempty(r)
            r = rank_estimation(m, n, tau, 'st', st, varargin{:});
        end
        idx = 1:r;
    case 'gamma'
        Y    = Ut(:, 1:mn) * diag(st) * Vt(:, 1:mn)';
        Y(Y < 0) = 0;
        llk  = @(y,x) sum(sum(iif(x > 0, - L * log(x) - L * y ./ x, -inf)));
        df   = (sqrt(m) + sqrt(n))^2/2;
        idx  = zeros(mn, 1);
        aicY = -2 * llk(Y, max(Y, eps)) + 2 * mn * df;
        for k = 1:mn
            sh = st;
            sh(k) = 0;
            Xh = max(Ut(:, 1:mn) * diag(sh) * Vt(:, 1:mn)', eps);
            aicXh = -2 * llk(Y, Xh) + 2 * (mn - 1) * df;
            idx(k) = aicXh > aicY;
        end
        idx = find(idx == 1)';
    case 'poisson'
        Y    = Ut(:, 1:mn) * diag(st) * Vt(:, 1:mn)';
        Y(Y < 0) = 0;
        llk = @(y,x) sum(sum(iif(x >= 0, ylogx(y, x) - x, -inf)));
        df   = (sqrt(m) + sqrt(n))^2/2;
        idx  = zeros(mn, 1);
        aicY = -2 * llk(Y, max(Y, eps)) + 2 * mn * df;
        for k = 1:mn
            sh = st;
            sh(k) = 0;
            Xh = max(Ut(:, 1:mn) * diag(sh) * Vt(:, 1:mn)', eps);
            aicXh = -2 * llk(Y, Xh) + 2 * (mn - 1) * df;
            idx(k) = aicXh > aicY;
        end
        idx = find(idx == 1)';
end
