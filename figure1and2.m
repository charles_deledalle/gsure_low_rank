clear all
close all

addpath('tools');
deterministic('on');

%%% Number of simulations for Monte-Carlo
M = 100;

%%% Number of discretization points for sigma1
T = 20;

%%% Settings noise-free matrix
n = 100;
m = n;

tau = 1 / sqrt(m);

u1 = 1 - (linspace(0,1,n)' - 1/2).^2; u1 = u1 / norm(u1);
v1 = 1 - (linspace(0,1,n)' - 1/2).^2; v1 = v1 / norm(v1);
u1v1 = u1 * v1';


%% Setting of singular value estimators
sv_est.symb{1}   = '$\hat{\bf X}^1$';
sv_est.style{1}  = { 'Color', 'b' };
sv_est.opt{1}    = { 'class',     'identity' };

sv_est.symb{2}   = '$\hat{\bf X}_w^1$';
sv_est.style{2}  = { 'Color', 'g' };
sv_est.opt{2}    = { 'class',     'optweights', ...
                     'objective', 'sure' };

sv_est.symb{3}   = '$\hat{\bf X}_{\mathrm{soft}}^1$';
sv_est.style{3}  = { 'Color', 'c' };
sv_est.opt{3}    = { 'class',     'softtresholding', ...
                     'objective', 'sure' };

sv_est.symb{4}   = '$\hat{\bf X}_*^1$';
sv_est.style{4}  = { 'Color', 'm' };
sv_est.opt{4}    = { 'class',     'optweights', ...
                     'objective', 'nadakuditi' };

sv_est.symb{5}   = '${\bf X}_*^1$';
sv_est.style{5}  = { '--', 'Color', 'k', 'LineWidth', 2 };
sv_est.opt{5}    = { 'class',     'optweights', ...
                     'objective', 'asymptot' };

K                = length(sv_est.opt);

%%% Computation
si1_list = linspace(0.25, 5, T);
for k = 1:K
    sh1_list{k} = zeros(T, M);
    nh1_list{k} = zeros(T, M);
end
hw = waitbar(0);
for t = 1:T
    waitbar(t / T, hw);
    si1 = si1_list(t);
    X = si1 * u1v1;
    for q = 1:M
        Y = X + tau * randn(n, m);
        [Ut, st, Vt] = mysvd(Y);
        aset = sv_active_set([st(1); zeros(m-1, 1)], ...
                             'noise', 'gaussian', ...
                             'm', m, 'n', n, 'tau', tau);

        st1 = st(1);
        ut1 = Ut(:, 1);
        vt1 = Vt(:, 1);
        ut1vt1 = ut1 * vt1';

        for k = 1:K
            sh1 = sv_shrinkage_rank1(st, sv_est.opt{k}{:}, ...
                                     'noise',  'gaussian', ...
                                     'Ut', Ut, 'Vt', Vt, 'X', X, ...
                                     'si1', si1, 'aset', aset, ...
                                     'm',  m,  'n',  n,  'tau', tau);
            sh1_list{k}(t, q) = sh1;
            nh1_list{k}(t, q) = nmse(sh1 * ut1vt1, X);
        end
    end
end
close(hw);

%%% Display and save to /tmp/

% Figure 1
f = fancyfigure;
for k = 2:(K-1)
    subplot(2, K-2, k-1)
    a = [];
    for l = [1 k K]
        a = [a, plotuncertainty(si1_list, sh1_list{l}, sv_est.style{l}{:})];
    end
    axis image
    xlim([0.25, 5]);
    ylim([0, 5]);
    xlabel('Singular value $\sigma_1$ of $\bf X$');
    ylabel('Estimated singular value');
    box on
    h = fancylegend(a, sv_est.symb{[1, k, K]}, ...
               'Location', 'NorthWest');
end
for k = 2:(K-1)
    subplot(2, K-2, K-2 + k-1)
    a = [];
    for l = [1 k K]
        a = [a, plotuncertainty(si1_list, sh1_list{l} ./ sh1_list{1}, sv_est.style{l}{:})];
    end
    xlabel('Singular value $\sigma_1$ of $\bf X$');
    ylabel('Estimated weight');
    axis square
    xlim([0.25, 5]);
    ylim([0, 1.1]);
    box on
    h = fancylegend(a, sv_est.symb{[1, k, K]}, ...
                    'Location', 'SouthEast');
end
savesubfig(f, '/tmp/fig1');
waitfor(f);

% Figure 2
f = fancyfigure;
for i = 1:2
    for k = 2:K-1
        subplot(2, K-2, (i-1) * (K-2) + k-1);
        a = [];
        for l = [1 k K]
            a = [a, plotuncertainty(si1_list, nh1_list{l}, sv_est.style{l}{:})];
        end
        axis square
        if i == 1
            xlim([1, 5]);
            ylim([0, 4.5]);
        else
            xlim([0.25, 3]);
            ylim([0, 1.7]);
        end
        xlabel('Singular value $\sigma_1$ of $\bf X$');
        ylabel('NMSE');
        box on
        if i == 1
            h = fancylegend(a, sv_est.symb{[1, k, K]}, ...
                            'Location', 'NorthEast');
        else
            h = fancylegend(a, sv_est.symb{[1, k, K]}, ...
                            'Location', 'SouthWest');
        end
    end
end
savesubfig(f, '/tmp/fig2');
