clear all
close all

addpath('tools');
deterministic('on');

%%% Number of simulations for Monte-Carlo
M = 20;

%%% Load setting
setting_figure7;

%%% Computation
sst       = zeros(mn, M);
for k = 1:K
    ssh{k}       = zeros(mn, M);
end
hw = waitbar(0);
for q = 1:M
    waitbar(q / M, hw);

    % Generate noisy matrix
    Y = mypoissrnd(X);
    [Ut, st, Vt] = mysvd(Y);
    aset = sv_active_set(st, 'noise', 'poisson', ...
                         'Ut', Ut, 'Vt', Vt, 'eps', eps);

    % Average spectrum of input
    sst(:, q) = st;

    for k = 1:K
        % Estimates of singular values
        sh{k}    = sv_shrinkage(st, sv_est.opt{k}{:}, ...
                                'noise',  'poisson', ...
                                'Ut', Ut, 'Vt', Vt, 'X', X, ...
                                'm',  m,  'n',  n, ...
                                'aset',   aset, 'eps', eps);

        % Compute approximations from singular values
        Xh{k}    = max(Ut(:, 1:mn) * diag(sh{k}) * Vt(:, 1:mn)', eps);

        % Average spectrum of output
        ssh{k}(:, q) = svd(Xh{k});
    end
end
close(hw);

%%% Display and save to /tmp/

% Figure 7.k
set(0,'defaultTextInterpreter','latex');
f = figure;
a = loglog(si, 'r');
hold on;
a = [a plotuncertainty(1:mn, sst, 'Color', 'b')];
for k = 1:K
    a = [a plotuncertainty(1:mn, ssh{k}, sv_est.style{k}{:})];
end
ylim([si(3)/100 2*si(1)]);
h = legend(a, '$\bf X$', '$\bf Y$', sv_est.symb{:}, ...
           'Location', 'NorthEast');
xlabel('Indices')
ylabel('Singular values')
box on
set(h,'Interpreter','latex');
savefig(f, '/tmp/fig7k.fig');

waitfor(f);

% Figure 7.a-j
range = [min(log(X(:))), max(log(X(:)))];
f = fancyfigure;
subplot(2,6,1)
plotimage(log(X), range);
title('Original matrix');
subplot(2,6,2)
plotimage(log(Y), range);
title('Noisy matrix');
for k = 1:K
    subplot(2,6,2+k)
    plotimage(log(Xh{k}), range);
    title(sprintf('%s', sv_est.name{k}));
end
linkaxes
savesubfig(f, '/tmp/fig7a-j/');
