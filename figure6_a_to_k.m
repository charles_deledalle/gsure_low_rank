clear all
close all

addpath('tools');
deterministic('on');

%%% Number of simulations for Monte-Carlo
M = 100;

%%% Load setting
setting_figure6;

%%% Computation
mst       = zeros(mn, M);
for k = 1:K
    msh{k}       = zeros(mn, M);
end
hw = waitbar(0);
for q = 1:M
    waitbar(q / M, hw);

    % Generate noisy matrix
    L = 80;
    Y = mygamrnd(X, L);
    [Ut, st, Vt] = mysvd(Y);
    aset = sv_active_set(st, 'noise', 'gamma', ...
                         'Ut', Ut, 'Vt', Vt, 'L', L', 'eps', eps);

    % Average spectrum of input
    sst(:, q) = st;

    for k = 1:K
        % Estimates of singular values
        sh{k}    = sv_shrinkage(st, sv_est.opt{k}{:}, ...
                                'noise',  'gamma', ...
                                'Ut', Ut, 'Vt', Vt, 'X', X, ...
                                'm',  m,  'n',  n,  'L', L, ...
                                'aset',   aset, 'eps', eps);

        % Compute approximations from singular values
        Xh{k}    = max(Ut(:, 1:mn) * diag(sh{k}) * Vt(:, 1:mn)', eps);

        % Average spectrum of output
        ssh{k}(:, q) = svd(Xh{k});
    end
end
close(hw);

%%% Display and save to /tmp/

% Figure 6.k
set(0,'defaultTextInterpreter','latex');
f = figure;
a = loglog(si, 'r');
hold on;
a = [a plotuncertainty(1:mn, sst, 'Color', 'b')];
for k = 1:K
    a = [a plotuncertainty(1:mn, ssh{k}, sv_est.style{k}{:})]
end
ylim([si(5)/100 2*si(1)]);
h = legend(a, '$\bf X$', '$\bf Y$', sv_est.symb{:}, ...
           'Location', 'NorthEast');
xlabel('Indices')
ylabel('Singular values')
set(h,'Interpreter','latex');
savefig(f, '/tmp/fig6k.fig');

waitfor(f);

% Figure 6.a-j
range = [min(X(:)), max(X(:))];
f = fancyfigure;
subplot(2,6,1)
plotimage(X, range);
title('Original matrix');
subplot(2,6,2)
plotimage(Y, range);
title('Noisy matrix');
for k = 1:K
    subplot(2,6,2+k)
    plotimage(Xh{k}, range);
    title(sprintf('%s', sv_est.name{k}));
end
linkaxes
savesubfig(f, '/tmp/fig6a-j/');
