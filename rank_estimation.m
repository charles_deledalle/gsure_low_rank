function r = rank_estimation(m, n, tau, varargin)
%% Estimate the rank of a matrix in a Gaussian setting
%
% Inputs/Outputs
%
%    M          the number of rows of the matrix
%
%    N          the number of columns of the matrix
%
%    TAU        standard deviation of the Gaussian noise
%
%    R          the estimated rank
%
% Optional arguments (but sometimes mandatory according to the COST)
%
%    RANK_METHOD  a string being
%               - 'none'        min(m, n)
%               - 'eff'         effective rank
%               - 'bulk_edge'   rank collapsing the bulk edge
%               - 'gavish'      estimator of Gavish and Donoho
%               - 'star'        oracle rank
%
%    ST         the noisy singular values
%
%    SI         the noise-free singular values
%               (only required for oracle estimators)
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle & Jeremie Bigot
% Email charles-alban.deledalle@math.u-bordeaux.fr



%%
options        = makeoptions(varargin{:});
rank_method    = getoptions(options, 'rank_method', 'bulk_edge');

switch rank_method
    case 'none'
        r      = min(m, n);
    case 'eff'
        si     = getoptions(options, 'si', [], true);
        c      = n / m;
        r      = sum(si > c^(1/4));
    case 'bulk_edge'
        st     = getoptions(options, 'st', [], true);
        cplus  = tau * (sqrt(m) + sqrt(n));
        r      = sum(st > cplus);
    case 'gavish'
        st     = getoptions(options, 'st', [], true);
        c      = n / m;
        lambda = sqrt(2 * (c + 1) + ...
                      8 * c / ((c + 1) + sqrt(c^2 + 14 * c + 1)));
        st     = st / tau / sqrt(m);
        r      = sum(st > lambda);
    case 'star'
        si     = getoptions(options, 'si', [], true);
        r      = sum(si > 0);
end
