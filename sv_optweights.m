function sh = sv_optweights(st, varargin)
%% Estimate singular values with optimal weighted shrinkage
%
% Inputs/Outputs
%
%    ST         the noisy singular values
%
%    SH         the estimated singular values
%
%    NOISE      a string being 'gaussian', 'gamma' or 'poisson'
%
% Optional arguments (but sometimes mandatory according to the COST)
%
%    OBJECTIVE  a string being
%               - 'mse', 'sure'*, 'nadakuditi', 'pca'                   (Gaussian case)
%               - 'mse_gamma', 'gsure', 'kls', 'sukls'*, 'pca_gamma'    (Gamma case)
%               - 'mse_poisson', 'pure', 'kla', 'pukla'*, 'pca_poisson' (Poisson case)
%               Default are marked with a * and depends on the noise
%
%    M          the number of rows of the matrix
%
%    N          the number of columns of the matrix
%
%    Ut         the left singular vectors stored in columns
%
%    Vt         the right singular vectors stored in columns
%
%    X          the noise-free version of the matrix
%               (only required for oracle estimators)
%
%    ASET       the active set of singular values
%
%    TAU        standard deviation of the Gaussian noise
%
%    L          shape parameter of the Gamma distribution
%
%    eps        a priori on the smallest entry of the noise-free matrix
%
%    DELTA      a direction (same size as ST) for estimating some SURE-like costs
%               generated as (non random) Bernoulli distributed if not provided
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle & Jeremie Bigot
% Email charles-alban.deledalle@math.u-bordeaux.fr



%%
options   = makeoptions(varargin{:});
noise     = getoptions(options, 'noise', '');

%% Retrieve required arguments wrt noise model
switch noise
    case 'gaussian'
        objective = getoptions(options, 'objective', 'sure');
        tau       = getoptions(options, 'tau', [], true);
    case 'gamma'
        objective = getoptions(options, 'objective', 'sukls');
        L         = getoptions(options, 'L', [], true);
        eps       = getoptions(options, 'eps', [], true);
    case 'poisson'
        objective = getoptions(options, 'objective', 'pukla');
        eps       = getoptions(options, 'eps', [], true);
    otherwise
        objective = getoptions(options, 'objective', 'mse');
end

%% Retrieve required arguments wrt objectives
switch objective
    case {'sure', 'nadakuditi', 'pca'}
        m      = getoptions(options, 'm', [], true);
        n      = getoptions(options, 'n', [], true);
        mn     = min(m, n);
    otherwise
        X      = getoptions(options, 'X', [], true);
        Ut     = getoptions(options, 'Ut', [], true);
        Vt     = getoptions(options, 'Vt', [], true);
        m      = size(Ut, 1);
        n      = size(Vt, 1);
        mn     = min(m, n);
end

%% Retrieve active set
if isfield(options, 'aset')
    aset   = getoptions(options, 'aset', [], true);
else
    aset   = sv_active_set(st, varargin{:});
end
mask       = zeros(mn, 1);
mask(aset) = 1;

%% Define optimal weights
switch objective
    case 'mse'
        s         = diag(Ut' * X * Vt);
        sh        = zeros(mn, 1);
        sh(aset)  = s(aset);

    case 'sure'
        sh        = zeros(mn, 1);
        for k = aset
            sh(k) = max(st(k) - tau^2 ./ st(k) * ...
                        (1 + abs(m-n) + ...
                         2 * sum(st(k)^2 ./ (st(k)^2 - st([1:(k-1) (k+1):mn]).^2))), ...
                        0);
        end

    case 'nadakuditi'
        st        = st / tau / sqrt(m);
        c         = n/m;
        sh        = zeros(mn, 1);
        for k = aset
            sh(k) = sqrt((st(k)^2 - (c+1))^2 - 4 * c) / st(k);
        end
        sh        = sh * tau * sqrt(m);
        st        = st * tau * sqrt(m);

    case { 'pca', 'pca_gamma', 'pca_poisson' }
        sh        = zeros(mn, 1);
        sh(aset)  = st(aset);

    case { 'sukls', 'gsure', 'kls', 'mse_gamma',
           'pukla', 'pure',  'kla', 'mse_poisson' }

        % Function of the singular values
        fsh     = @(w)          w .* st;

        % Derivative wrt the sv
        fdsh    = @(w)          w;

        % Directional derivative wrt the sv in direction ds
        fddsh   = @(w, ds)      w .* ds;

        % Greedy optimization
        wh        = zeros(mn, 1);
        wh(aset)  = 1;
        for k = aset
            subfsh   = @(w)     fsh([wh(1:(k-1)); w; wh((k+1):mn)]);
            subfdsh  = @(w)     fdsh([wh(1:(k-1)); w; wh((k+1):mn)]);
            subfddsh = @(w, ds) fddsh([wh(1:(k-1)); w; wh((k+1):mn)], ds);
            cost     = sv_objective(subfsh, 'st', st, ...
                                    'fdsh', subfdsh, 'fddsh', subfddsh, ...
                                    varargin{:});
            wh(k)    = fminbnd(cost, 0, 1);
        end
        sh = fsh(wh);
    otherwise
        error(sprintf('Objective %s unknown', objective));
end
sh(sh > st) = st(sh > st);
