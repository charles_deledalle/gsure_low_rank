function savesubfig(h, dirname)
%% Save a figure and its subfigures to a directory
%
%  save the figure H in directory DIRNAME and all its subplots as
%    DIRNAME/main.fig
%    DIRNAME/subplot1.fig
%    DIRNAME/subplot2.fig
%    ...
%
%  for subaxes containing image data, SAVEFIGSUB will also create
%    DIRNAME/subimg1.png
%    DIRNAME/subimg2.png
%    ...
%
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle & Jeremie Bigot
% Email charles-alban.deledalle@math.u-bordeaux.fr



[status, msg,~] = mkdir(dirname);
if ~status
    error(sprintf('Cannot create directory "%s": %s', dirname, msg));
end

% Prevent figure to be closed while saveing it
hcloser = get(h, 'closer');
set(h, 'closer', '');

%
savefig(h, [ dirname '/' 'main.fig' ]);
hchildren = get(h, 'children');
K = length(hchildren);
k = K;
i = 1;
while k > 0
    if strcmp(get(hchildren(k), 'type'), 'axes')
        haxe = hchildren(k);
        [img, flag] = getimage(haxe);
        switch flag
            case 0 % Not an image
            case 1 % Indexed image
                warning('case 1 not implemented yet')
            case 2 % Intensity image in standard range
                imwrite(img, [ dirname '/' sprintf('subimg%d.png', i) ]);
            case 3 % Intensity image not in standard range
                switch get(haxe, 'climmode')
                    case 'manual'
                        range = get(haxe, 'clim');
                    case 'auto'
                        range = [min(img(:)) max(img(:))];
                end
                img = (img - range(1)) / (range(2) - range(1));
                imwrite(img, [ dirname '/' sprintf('subimg%d.png', i) ]);
            case 4 % RGB Image
                imwrite(img, [ dirname '/' sprintf('subimg%d.png', i) ]);
            case 5 % Binary image
                warning('case 5 not implemented yet')
        end
        hsubfig = figure('visible', 'off');
        set(hsubfig,'CreateFcn','set(gcf,''Visible'',''on'')')
        haxe_list = haxe;
        while k - 1 > 0 && ~strcmp(get(hchildren(k - 1), 'type'), 'axes')
            switch get(hchildren(k - 1), 'type')
                case { 'legend', 'colorbar' }
                    haxe_list = [hchildren(k - 1) haxe];
                otherwise
                    warning(sprintf('cannot export "%s"', ...
                                    get(hchildren(k - 1), 'type')));
            end
            k = k - 1;
        end
        haxe_new = copyobj(haxe_list, hsubfig);
        set(haxe_new(end), 'Position', get(0, 'DefaultAxesPosition'));
        colormap(hsubfig, colormap(h));
        filename = [ dirname '/' sprintf('subplot%d.fig', i) ];
        savefig(hsubfig, filename);
        delete(hsubfig);
        i = i + 1;
    end
    k = k - 1;
end

% Restore closing property
set(h, 'closer', hcloser);
disp(sprintf('subfigures saved to "%s"', dirname));
