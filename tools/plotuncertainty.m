function h = plotuncertainty(x, y, varargin)
%% Plot median curve with confindence interval
%
% Inputs/Outputs
%
%    X          a list of T values in x-axis
%    Y          an array of T x K values for y-axis
%    H          an handle on the curve
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle & Jeremie Bigot
% Email charles-alban.deledalle@math.u-bordeaux.fr



color = nan;
for k = 1:length(varargin)
    if ischar(varargin{k}) && strcmp(varargin{k}, 'Color')
        if k + 1 <= length(varargin)
            color = varargin{k+1};
        end
    end
end
if isnan(color)
    error('At least the color should be given')
end

qmin = quantile(y, 0.1);
qmax = quantile(y, 0.9);
if qmin >= qmax
    a    = max(y(:)) - min(y(:));
    qmin = quantile(y, 0.1)-a*1e-7;
    qmax = quantile(y, 0.9)+a*1e-7;
end
x = x(:);

hp = patch([x; x(end:-1:1); x(1)], ...
           [qmax; qmin(end:-1:1); qmax(1)], ...
           zeros(2*length(x)+1, 1), ...
           col2rgb(color), 'EdgeColor', 'none');
set(hp, 'facealpha', .2)
set(hp, 'edgealpha', .2)
hold on
h = plot(x, median(y, 2), varargin{:});
if nargout == 0
    clear h
end
