clear all
close all

addpath('tools');
deterministic('on');

%%% Number of simulations for Monte-Carlo
M = 1000;

%%% Number of discretization points for the rank
T = 20;

%%% Load setting
setting_figure8;

%%% Computation
rank_list     = unique(round(linspace(1, mn, T)));
T             = length(rank_list);
for rho = [6 4 2]

    error_init       = zeros(T, M);
    for k = 1:K
        error_est{k} = zeros(T, M);
    end
    for t = 1:T
        r = rank_list(t);

        fprintf('rho = %d, r = %d\n', rho, r)

        %%% Modify true spectrum
        si            = si_original;
        si((r+1):end) = 0;
        c             = n/m;
        si(1:r)       = rho*(c^(1/4));
        X             = U(:, 1:mn) * diag(si) * V(:, 1:mn)';

        %%% Monte-Carlo simulations
        for q = 1:M
            % Generate noisy matrix
            tau          = 1 / sqrt(m);
            Y            = X + tau * randn(m, n);
            [Ut, st, Vt] = mysvd(Y);

            % Optimal rank MSE and KLA
            rh = rank_estimation(m, n, tau, 'st', st);

            % Compute initial error
            error_init(:, q) = nmse(Y, X);

            for k = 1:K
                % Estimates of singular values
                sh{k} = sv_shrinkage(st, sv_est.opt{k}{:}, ...
                                     'noise', 'gaussian', ...
                                     'Ut', Ut, 'Vt', Vt, 'X', X, ...
                                     'm',  m,  'n',  n,  'tau', tau, ...
                                     'rank', rh);

                % Compute approximations from singular values
                Xh{k} = Ut(:, 1:mn) * diag(sh{k}) * Vt(:, 1:mn)';
                error_est{k}(t, q)  = nmse(Xh{k}, X);
            end
        end
    end

    %%% Display and save to /tmp/
    f = fancyfigure;
    for i = 1:3
        subplot(1, 3, i);
        switch i
            case 1
                a = [];
                for k = 1:K
                    a = [a, plotmedian(rank_list, error_est{k}, sv_est.style{k}{:})];
                end
                h = legend(a, sv_est.symb{:}, 'Location', 'NorthEast');
            case 2
                a = [];
                for k = 5:6
                    a = [a, plotuncertainty(rank_list, error_est{k}, sv_est.style{k}{:})];
                end
                h = legend(a, sv_est.symb{5:6}, 'Location', 'NorthEast');
            case 3
                a = [];
                for k = 1:4
                    a = [a, plotuncertainty(rank_list, error_est{k}, sv_est.style{k}{:})];
                end
                h = legend(a, sv_est.symb{1:4}, 'Location', 'NorthEast');
        end
        set(h,'Interpreter','latex');
        ylabel('NMSE');
        xlabel('True rank');
        xlim([1, mn]);
        box on;
        if i == 1
            yl = ylim;
        else
            ylim(yl);
        end
        axis square
    end
    savesubfig(f, sprintf('/tmp/fig8_rho%d', rho));
    waitfor(f);
end
