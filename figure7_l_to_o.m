clear all
close all

addpath('tools');
deterministic('on');

%%% Number of simulations for Monte-Carlo
M = 100;

%%% Number of discretization points of the rank
T = 20;

%%% Apply restriction to the active set
%active_set_estimation = 'none';
active_set_estimation = 'aic';

%%% Load setting
setting_figure7;

%%% Computation
rank_list     = unique(round(logspace(log10(1), log10(mn), T)));
T             = length(rank_list);
nmse_init     = zeros(T, M);
mkla_init     = zeros(T, M);
for k = 1:k
    nmse_est{k} = zeros(T, M);
    mkla_est{k} = zeros(T, M);
end
hw = waitbar(0);
for q = 1:M
    waitbar(q / M, hw);

    % Generate noisy matrix
    Y = mypoissrnd(X);
    [Ut, st, Vt] = mysvd(Y);
    if strcmp(active_set_estimation, 'aic')
        aset = sv_active_set(st, 'noise', 'poisson', ...
                             'Ut', Ut, 'Vt', Vt, 'eps', eps);
    else
        aset = 1:mn;
    end
    % Compute initial error
    nmse_init(:, q)    = nmse(max(Y, eps), X);
    mkla_init(:, q)    = mkla(max(Y, eps), X);

    for k = 1:K
        % Estimates of singular values
        sh{k}    = sv_shrinkage(st, sv_est.opt{k}{:}, ...
                                'noise', 'poisson', ...
                                'Ut', Ut, 'Vt', Vt, 'X', X, ...
                                'm',  m,  'n',  n, ...
                                'eps', eps, 'aset', aset);

        % Compute approximations from singular values
        if ~strcmp(sv_est.opt{k}{2}, 'optweights')
            Xh{k} = max(Ut(:, 1:mn) * diag(sh{k}) * Vt(:, 1:mn)',eps);
            nmse_est{k}(:, q)  = nmse(Xh{k}, X);
            mkla_est{k}(:, q)  = mkla(Xh{k}, X);
        else
            for i = 1:T
                r = rank_list(i);
                Xh{k} = max(Ut(:, 1:r) * diag(sh{k}(1:r)) * Vt(:, 1:r)', eps);
                nmse_est{k}(i, q)  = nmse(Xh{k}, X);
                mkla_est{k}(i, q) = mkla(Xh{k}, X);
            end
        end
    end
end
close(hw);

%%% Display and save to /tmp/

f = fancyfigure;

% Figure 7.l-m
subplot(1, 2, 1);
plotuncertainty(rank_list, nmse_init, 'Color', 'b');
for k = 1:K
    plotuncertainty(rank_list, nmse_est{k}, sv_est.style{k}{:});
end
switch active_set_estimation
    case 'none'
        xlabel('Rank $r$ (no restriction)')
    case 'aic'
        xlabel('Rank $r$ (restricted to the active set)')
end
ylabel('NMSE')
xlim([1, mn])
ylim([1.5*10^-5 5*10^-4]);
box on;
set(gca,'XScale','log','YScale','log')

% Figure 7.n-o
subplot(1, 2, 2);
hold off
plotuncertainty(rank_list, mkla_init, 'Color', 'b');
for k = 1:K
    plotuncertainty(rank_list, mkla_est{k}, sv_est.style{k}{:});
end
switch active_set_estimation
    case 'none'
        xlabel('Rank $r$ (no restriction)')
    case 'aic'
        xlabel('Rank $r$ (restricted to the active set)')
end
ylabel('MKLA')
xlim([1, mn])
ylim([1.7*10^3 4*10^4]/m/n);
box on;
set(gca,'XScale','log','YScale','log')

switch active_set_estimation
    case 'none'
        savesubfig(f, '/tmp/fig7_m_and_o');
    case 'aic'
        savesubfig(f, '/tmp/fig7_l_and_n');
end
