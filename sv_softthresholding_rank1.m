function sh = sv_softthresholding_rank1(st, varargin)
%% Estimate singular value of matrix of rank 1 with Soft Thresholding
%
% Inputs/Outputs
%
%    ST         noisy singular values
%    SH         estimated singular values
%
% Optional arguments
%
%    See sv_objective
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle & Jeremie Bigot
% Email charles-alban.deledalle@math.u-bordeaux.fr



mn      = length(st);
st1     = st(1);

% Define active set
mask    = ones(mn, 1);

% Function of the singular values
fsh     = @(lambda)     mask .* [ max(st1 - lambda, 0); zeros(mn-1, 1) ];

% Derivative wrt the sv
fdsh    = @(lambda)     mask .* [ iif(st1 >= lambda, 1, 0); zeros(mn-1, 1) ];

% Directional derivative wrt the sv in direction ds
fddsh   = @(lambda, ds) mask .* [ iif(st1 >= lambda, ds(1), 0); zeros(mn-1, 1) ];

% Create cost functions
cost    = sv_objective(fsh, 'st', st, 'fdsh', fdsh, 'fddsh', fddsh, varargin{:});

% Minimiwe cost function in the range [0 2max(st)]
lambda  = fminbnd(cost, 0, max(st1));

% Return optimal estimate
sh      = fsh(lambda);
sh      = sh(1);
