clear all
close all

addpath('tools');
deterministic('on');

%%% Number of simulations for Monte-Carlo
M = 100;

%%% Apply restriction to the bulk edge
%rank_method = 'none';
rank_method = 'bulk_edge';
%rank_method = 'gavish';
%rank_method = 'star';
%rank_method = 'eff';

%%% Load settings
setting_figure5;

%%% Computation
error_init       = zeros(mn, M);
for k = 1:K
    error_est{k} = zeros(mn, M);
end
hw = waitbar(0);
for q = 1:M
    waitbar(q / M, hw);

    % Generate noisy matrix
    tau          = 80;
    Y            = X + tau * randn(m, n);
    [Ut, st, Vt] = mysvd(Y);

    % Compute initial error
    error_init(:, q)    = nmse(Y, X);

    % Optimal rank MSE and KLA
    rh = rank_estimation(m, n, tau, 'st', st, 'si', si, ...
                         'rank_method', rank_method);

    for k = 1:K
        % Estimates of singular values
        sh{k}    = sv_shrinkage(st, sv_est.opt{k}{:}, ...
                                'noise', 'gaussian', ...
                                'Ut', Ut, 'Vt', Vt, 'X', X, ...
                                'm',  m,  'n',  n,  'tau', tau, ...
                                'rank', rh);

        % Compute approximations from singular values
        if ~strcmp(sv_est.opt{k}{2}, 'optweights')
            Xh{k} = Ut(:, 1:mn) * diag(sh{k}) * Vt(:, 1:mn)';
            error_est{k}(:, q)  = nmse(Xh{k}, X);
        else
            for i = 1:mn
                Xh{k} = Ut(:, 1:i) * diag(sh{k}(1:i)) * Vt(:, 1:i)';
                error_est{k}(i, q)  = nmse(Xh{k}, X);
            end
        end
    end
end
close(hw);

%%% Display and save to /tmp/

% Figure 5.j
f = fancyfigure;
plotuncertainty(1:mn, error_init, 'Color', 'b');
for k = 1:K
    plotuncertainty(1:mn, error_est{k}, sv_est.style{k}{:});
end
switch rank_method
  case 'none'
    xlabel('Rank $r$ (no restriction)')
  case 'bulk_edge'
    xlabel('Rank $r$ (restricted to the bulk edge)')
  case 'gavish'
    xlabel('Rank $r$ (restricted as in [GD14b])')
  case 'star'
    xlabel('Rank $r$ (restricted to the oracle/true rank)')
  case 'eff'
    xlabel('Rank $r$ (restricted to the effective rank [Nad14])')
end
ylabel('NMSE')
xlim([1, mn])
ylim([10^-3/3 2*10^-2]);
box on
set(gca,'XScale','log','YScale','log')
savefig(f, sprintf('/tmp/fig5%s.fig', rank_method));
