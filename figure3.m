clear all
close all

addpath('tools');
deterministic('on');

%%% Number of simulations for Monte-Carlo
M = 100;

%%% Number of discretization points for sigma1
T = 20;

%%% Settings
n = 100;
m = n;

L = 3;

u1 = 1 - (1./(1:n)' - 1/2).^2; u1 = u1 / norm(u1);
v1 = 1 - (1./(1:m)' - 1/2).^2; v1 = v1 / norm(v1);
u1v1 = u1 * v1';

%%% Setting of singular value estimators
sv_est.symb{1}   = '$\hat{\bf X}^1$';
sv_est.style{1}  = { 'Color', 'b' };
sv_est.opt{1}    = { 'class',     'identity' };

sv_est.symb{2}   = 'GSURE $\hat{\bf X}_w^1$';
sv_est.style{2}  = { 'Color', 'g' };
sv_est.opt{2}    = { 'class',     'optweights', ...
                     'objective', 'gsure' };

sv_est.symb{3}   = 'GSURE $\hat{\bf X}_{\mathrm{soft}}^1$';
sv_est.style{3}  = { 'Color', 'c' };
sv_est.opt{3}    = { 'class',     'softtresholding', ...
                     'objective', 'gsure' };

sv_est.symb{4}   = 'SUKLS $\hat{\bf X}_w^1$';
sv_est.style{4}  = { 'Color', 'k' };
sv_est.opt{4}    = { 'class',     'optweights', ...
                     'objective', 'sukls' };

sv_est.symb{5}   = 'SUKLS $\hat{\bf X}_{\mathrm{soft}}^1$';
sv_est.style{5}  = { 'Color', 'm' };
sv_est.opt{5}    = { 'class',     'softtresholding', ...
                     'objective', 'sukls' };

eps              = 1e-6;

K                = length(sv_est.opt);

%%% Computation
si1_list    = linspace(0.1, 5, T);
for k = 1:K
    sh1_list{k} = zeros(T, M);
    nh1_list{k} = zeros(T, M);
    mh1_list{k} = zeros(T, M);
end
hw = waitbar(0);
for t = 1:T
    waitbar(t / T, hw);
    si1 = si1_list(t);
    X = si1 * u1v1;
    for q = 1:M
        Y = mygamrnd(X, L);
        [Ut, st, Vt] = mysvd(Y);
        aset = sv_active_set([st(1); zeros(m-1, 1)], ...
                             'noise', 'gamma', ...
                             'Ut', Ut, 'Vt', Vt, 'L', L, 'eps', eps);

        st1 = st(1);
        ut1 = Ut(:, 1);
        vt1 = Vt(:, 1);
        ut1vt1 = ut1 * vt1';

        for k = 1:K
            sh1 = sv_shrinkage_rank1(st, sv_est.opt{k}{:}, ...
                                     'noise',  'gamma', ...
                                     'Ut', Ut, 'Vt', Vt, 'X', X, ...
                                     'si1', si1, 'aset', aset, ...
                                     'm',  m,  'n',  n,  'L', L, ...
                                     'eps', eps);
            sh1_list{k}(t, q) = sh1;
            nh1_list{k}(t, q) = nmse(sh1 * ut1vt1, X);
            mh1_list{k}(t, q) = mkls(sh1 * ut1vt1, X, L);
        end
    end
end
close(hw);

%%% Display and save to /tmp/

f = fancyfigure;

% Figure 3.a
subplot(2, 4, 1);
a = [];
for k = 1:3
    a = [a, plotuncertainty(si1_list, sh1_list{k}, sv_est.style{k}{:})];
end
axis image
xlim([0, 5]);
ylim([0, 5]);
set(gca, 'XTick', [1 2 3 4])
set(gca, 'YTick', [1 2 3 4])
set(gca, 'YTickLabel', {'1.0' '2.0' '3.0' '4.0'})
xlabel('Singular value $\sigma_1$ of $\bf X$');
ylabel('Estimated singular value');
box on

% Figure 3.b
subplot(2, 4, 2);
a = [];
for k = 1:3
    a = [a, plotuncertainty(si1_list, sh1_list{k}./sh1_list{1}, sv_est.style{k}{:})];
end
xlim([0.25, 5]);
ylim([0.5 1.05]);
set(gca, 'XTick', [1 2 3 4])
set(gca, 'YTick', [0.6 0.8 1])
axis square
xlabel('Singular value $\sigma_1$ of $\bf X$');
ylabel('Estimated weight');
box on

% Figure 3.c
subplot(2, 4, 3);
a = [];
for k = 1:3
    a = [a, plotuncertainty(si1_list, nh1_list{k}, sv_est.style{k}{:})];
end
xlim([0.25, 5]);
ylim([0, 1.7/10]);
set(gca, 'XTick', [1 2 3 4])
set(gca, 'YTick', [0 0.1])
axis square
xlabel('Singular value $\sigma_1$ of $\bf X$');
ylabel('NMSE');
box on

% Figure 3.d
subplot(2, 4, 4);
a = [];
for k = 1:3
    a = [a, plotuncertainty(si1_list, mh1_list{k}, sv_est.style{k}{:})];
end
xlim([0.25, 5]);
ylim([0, 2.4/10]);
set(gca, 'XTick', [1 2 3 4])
set(gca, 'YTick', [0 0.1 0.2])
axis square
xlabel('Singular value $\sigma_1$ of $\bf X$');
ylabel('MKLS');
h = legend(a, sv_est.symb{1:3}, ...
           'Location', 'NorthEast');
set(h,'Interpreter','latex');
box on

% Figure 3.a
subplot(2, 4, 5);
a = [];
for k = [1 4:5]
    a = [a, plotuncertainty(si1_list, sh1_list{k}, sv_est.style{k}{:})];
end
axis image
xlim([0, 5]);
ylim([0, 5]);
set(gca, 'XTick', [1 2 3 4])
set(gca, 'YTick', [1 2 3 4])
set(gca, 'YTickLabel', {'1.0' '2.0' '3.0' '4.0'})
xlabel('Singular value $\sigma_1$ of $\bf X$');
ylabel('Estimated singular value');
box on

% Figure 3.b
subplot(2, 4, 6);
a = [];
for k = [1 4:5]
    a = [a, plotuncertainty(si1_list, sh1_list{k}./sh1_list{1}, sv_est.style{k}{:})];
end
xlim([0.25, 5]);
ylim([0.5 1.05]);
set(gca, 'XTick', [1 2 3 4])
set(gca, 'YTick', [0.6 0.8 1])
axis square
xlabel('Singular value $\sigma_1$ of $\bf X$');
ylabel('Estimated weight');
box on

% Figure 3.c
subplot(2, 4, 7);
a = [];
for k = [1 4:5]
    a = [a, plotuncertainty(si1_list, nh1_list{k}, sv_est.style{k}{:})];
end
xlim([0.25, 5]);
ylim([0, 1.7/10]);
set(gca, 'XTick', [1 2 3 4])
set(gca, 'YTick', [0 0.1])
axis square
xlabel('Singular value $\sigma_1$ of $\bf X$');
ylabel('NMSE');
box on

% Figure 3.d
subplot(2, 4, 8);
a = [];
for k = [1 4:5]
    a = [a, plotuncertainty(si1_list, mh1_list{k}, sv_est.style{k}{:})];
end
xlim([0.25, 5]);
ylim([0, 2.4/10]);
set(gca, 'XTick', [1 2 3 4])
set(gca, 'YTick', [0 0.1 0.2])
axis square
xlabel('Singular value $\sigma_1$ of $\bf X$');
ylabel('MKLS');
h = legend(a, sv_est.symb{[1 4:5]}, ...
           'Location', 'NorthEast');
set(h,'Interpreter','latex');
box on

savesubfig(f, '/tmp/fig3');
